from time import perf_counter as clock

t1 = clock()
a = []
for i in range(10**7):
    a.append(1.)
t2 = clock()
print(t2-t1)

t1 = clock()
a = [1. for i in range(10**7)]
t2 = clock()
print(t2-t1)
