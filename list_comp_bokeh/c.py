from time import perf_counter as clock
from collections import defaultdict

res = defaultdict(list)
for q in range(24):
    n = 2**q
    res['x'].append(n)

    t1 = clock()
    a = []
    for i in range(n):
        a.append(1.)
    t2 = clock()
    res['for'].append(t2-t1)
    print(n, t2-t1)

    t1 = clock()
    a = [1. for i in range(n)]
    t2 = clock()
    res['list_comp'].append(t2-t1)
    print(n, t2-t1)
    if res['for'][-1] > 1:
        break

import numpy as np
np.savez('res.npz', **res)
