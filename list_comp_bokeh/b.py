from time import perf_counter as clock

x, res1, res2 = [], [], []
for q in range(24):
    n = 2**q
    x.append(n)

    t1 = clock()
    a = []
    for i in range(n):
        a.append(1.)
    t2 = clock()
    res1.append(t2-t1)
    print(n, t2-t1)

    t1 = clock()
    a = [1. for i in range(n)]
    t2 = clock()
    res2.append(t2-t1)
    print(n, t2-t1)
    if t2-t1 > 1:
        break

import numpy as np
np.save('x.npy', x)
np.save('res1.npy', res1)
np.save('res2.npy', res2)
