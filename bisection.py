import sys
from random import randint

def find(a, x):
    """
    Ищет элемент x в отсортированном списке a при помощи
    бинарного поиска. Возвращает индекс найденного
    элемента (любого, если их в списке несколько),
    или -1, если элемент не найден.
    """
    return

def eq(x, y):
    if x == y:
        sys.stdout.write('.')
        sys.stdout.flush()
    else:
        print(f'\nexpected: {y}, got: {x}')
        import ipdb; ipdb.set_trace()

def test1():
    print('test1')
    a = [3, 4, 7, 9, 10, 11, 15]
    for i, v in enumerate(a):
        eq(find(a, v), i)
    print('ok')

def test2():
    print('test2')
    a = [3, 4]
    for i, v in enumerate(a):
        eq(find(a, v), i)
    print('ok')

def test_missing():
    print('test missing')
    a = [3, 4, 7, 9, 10, 11, 15]
    for i in range(a[-1] + 1):
        if i in a:
            eq(a[find(a, i)], i)
        else:
            eq(find(a, i), -1)
    print('ok')

def rand_test():
    print('rand test')
    a = []
    for i in range(100):
        a.append(randint(0, 999))
    a.sort()
    for j in range(100):
        x = randint(0, 999)
        q = find(a, x)
        if q == -1 and x not in a:
            pass
        elif q != -1 and x in a:
            if a[q] != x:
                print(f'q = {q}, a[q] = {a[q]}, x={x}')
        else:
            print(f'q={q}, x={x}')
        sys.stdout.write('.')
        sys.stdout.flush()
    print('ok')

test1()
test2()
test_missing()
rand_test()
