#!/usr/bin/env python3

from random import random

# Эти задачи нужно решить в одну строчку при помощи list comprehensions

def test(got, expected):
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
    print('%s Получено: %s | Ожидалось: %s' % 
          (prefix, repr(got), repr(expected)))

# D. Модифицируйте последнюю функцию из list_comprehensions1.py так, чтобы она подсчитывала  
# количество пар "взаимо-палиндромных" чисел (например, 125 и 521) из данного диапазона.  
# Например, в диапазоне между 10 и 40 их три: (12,21), (13,31) и (23,32)
def palindromes2(a, b):
    return 0

def test_palindromes2():
    test(palindromes2(10, 40), 3)
    test(palindromes2(12, 21), 1)
    test(palindromes2(13, 21), 0)
    test(palindromes2(1, 100), 36)
    test(palindromes2(1, 1000), 396)

# E. Вычислите число π методом Монте-Карло: на квадрате с единичной стороной строится 
# четверь окружности единичного радиуса, затем последовательно кидаются n точек, со случайными,
# равномерно распределёнными по площади квадрата, координатами. Количество точек, 
# попавших внутрь окружности, пропорционально числу π.
# Подсказка: функция random() генерирует случайное число в диапазоне [0, 1)
 
def calc_pi(n):
    return 0

def test_calc_pi():
    print(calc_pi(100))
    print(calc_pi(1000))
    print(calc_pi(10000))
    print(calc_pi(100000))
    print(calc_pi(1000000))

# F. Реализуйте функцию, подсчитывающую полное количество слов в файле.  
# Файл может быть больше, чем объём оперативной + виртуальной памяти,  
# поэтому читать из него надо построчно. 

def total_words(filename):
    return 0

def test_total_words():
    test(total_words('alice-clean.txt'), 26409)

# Для отладки можно использовать файл alice-clean.txt
# http://lmaximov.bitbucket.org/alice-clean.txt

if __name__ == '__main__':
    test_palindromes2()
    test_calc_pi()
    test_total_words()

