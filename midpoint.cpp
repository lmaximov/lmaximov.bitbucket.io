#include <stdio.h>
#include <iostream>
#include <time.h>
#include <math.h>
#include <stdlib.h>
//#include <random>
//#include <functional>


double f(double t) {
	return 3 * t*t*exp(pow(t, 3));
}

double midpoint(double a, double b, int n) {
	double h = (b - a) / n;
	double result = 0;
	for (int i = 0; i<n; i++)
		result += f(a + h / 2.0 + i*h);
	return result*h;
}

#define A 2
#define B 24
#define TRUTH (exp(1.)-1.)

double res[B - A], times[B - A];
void main() {
	clock_t start, stop;
	for (int i = 0; i<B - A; i++) {
		start = clock();
		res[i] = midpoint(0, 1, 1 << (A + i));
		stop = clock();
		times[i] = double(stop - start) / CLOCKS_PER_SEC;
		//		printf("%d %lf %lf\n", i, res[i] - TRUTH, times[i]);
		printf("%d %lf\n", A + i, times[i]);
	}
	return;
}