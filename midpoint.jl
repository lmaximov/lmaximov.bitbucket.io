function midpoint_vector(f, a, b, n)
    local t1 = time_ns()
    h = (b-a)/n
    x = linspace(a + h/2, b - h/2, n)
    y = h*sum(f.(x))
    local t2 = time_ns()
    y, (t2-t1)/1e9
end

function midpoint(f, a, b, n)
    local t1 = time_ns()
    h = (b-a)/n
    result = 0
    for i = 0:n-1
        result += f(a + h/2.0 + i*h)
	end
    local t2 = time_ns()
	result*h, (t2-t1)/1e9
end

f(t) = 3*t^2*exp(t^3)

truth = exp(1) - 1

for i = 2:20
	println(i, " ", midpoint_vector(f, 0, 1, 2^i)[2])
end

println()

for i = 2:20
	println(i, " ", midpoint(f, 0, 1, 2^i)[2])
end

